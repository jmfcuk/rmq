﻿using RabbitMQ.Client;
using System;
using System.Configuration;
using System.Text;

namespace SlamIt
{
    class Program
    {
        private const string _EXCHANGE = "EXCHANGE";
        private const string _EXCHANGE_TYPE = "topic";

        static void Main(string[] args)
        {
            ulong max = 1L;
            if (args.Length > 0)
                max = ulong.Parse(args[0]);

            ConnectionFactory factory = GetConnectionFactory();

            using (IConnection connection = factory.CreateConnection())
            using (IModel channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _EXCHANGE,
                                        type: _EXCHANGE_TYPE);
                int topicNo = 1;
                //for (ulong x = 0; x < max; x++)
                while(true)
                {
                    string topic = $"topic{topicNo}";

                    //string message = $"test message {x}";
                    string message = $"test message...";
                    byte[] body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: _EXCHANGE,
                                         routingKey: topic,
                                         basicProperties: null,
                                         body: body);

                    Console.WriteLine("Sent '{0}':'{1}'", topic, message);

                    topicNo++;
                    if (topicNo > 3)
                        topicNo = 1;
                }
            }
        }

        private static ConnectionFactory GetConnectionFactory()
        {
            ConnectionFactory cf = new ConnectionFactory();
            cf.HostName = ConfigurationManager.AppSettings["rmqHost"];
            cf.UserName = ConfigurationManager.AppSettings["rmqUid"];
            cf.Password = ConfigurationManager.AppSettings["rmqPwd"];
            cf.Port = AmqpTcpEndpoint.UseDefaultPort; // 5672

            return cf;
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("Pub <topic> <message>");
        }
    }
}
