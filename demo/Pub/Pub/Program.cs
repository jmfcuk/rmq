﻿using RabbitMQ.Client;
using System;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Pub
{
    class Program
    {
        private const string _EXCHANGE = "JOHNS_EXCHANGE";
        private const string _EXCHANGE_TYPE = "topic";

        static void Main(string[] args)
        {
            if (args == null || args.Length < 2)
            {
                PrintUsage();
                Environment.ExitCode = 0;
                return;
            }

            ConnectionFactory factory = GetConnectionFactory();

            using (IConnection connection = factory.CreateConnection())
            using (IModel channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _EXCHANGE,
                                        type: _EXCHANGE_TYPE);

                string routingKey = args[0];
                string message = string.Join(" ", args.Skip(1).ToArray());
                byte[] body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: _EXCHANGE,
                                     routingKey: routingKey,
                                     basicProperties: null,
                                     body: body);
                
                Console.WriteLine("Sent '{0}':'{1}'", routingKey, message);
            }
        }

        private static ConnectionFactory GetConnectionFactory()
        {
            ConnectionFactory cf = new ConnectionFactory();
            cf.HostName = ConfigurationManager.AppSettings["rmqHost"];
            cf.UserName = ConfigurationManager.AppSettings["rmqUid"];
            cf.Password = ConfigurationManager.AppSettings["rmqPwd"];
            cf.Port = AmqpTcpEndpoint.UseDefaultPort; // 5672

            return cf;
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("Pub <topic> <message>");
        }
    }
}
