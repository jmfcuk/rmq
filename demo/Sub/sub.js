var amqp = require('amqplib/callback_api');

var args = process.argv.slice(2);

if (args.length == 0) {
  console.log("Usage: sub.js <topic>");
  process.exit(0);
}

amqp.connect('amqp://admin:admin@hnap70', function(err, conn) {

  conn.createChannel(function(err, ch) {

    var ex = 'JOHNS_EXCHANGE';

    ch.assertExchange(ex, 'topic', {durable: false});

    ch.assertQueue('', {exclusive: true}, function(err, q) {

      console.log('q = %s', q.queue);

      console.log('[*] Waiting for messages. To exit press CTRL+C');

      args.forEach(function(key) {
        console.log('bind topic to queue: %s -> %s', q.queue, key);
        ch.bindQueue(q.queue, ex, key);
      });

      ch.consume(q.queue, function(msg) {

        console.log("[x] %s: '%s'", msg.fields.routingKey, msg.content.toString());
      }, {noAck: true});
    });
  });
});

