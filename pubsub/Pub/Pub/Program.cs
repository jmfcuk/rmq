﻿using RabbitMQ.Client;
using System;
using System.Linq;
using System.Text;

namespace Pub
{
    class Program
    {
        private static string _EXCHANGE = "all_topics";

        static void Main(string[] args)
        {
            var factory = 
                new ConnectionFactory() { HostName = "localhost" };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _EXCHANGE,
                                        type: "direct");

                var topic = (args.Length > 0) ? args[0] : "info";

                var message = (args.Length > 1)
                              ? string.Join(" ", args.Skip(1).ToArray())
                              : "Hello World!";

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: _EXCHANGE,
                                     routingKey: topic,
                                     basicProperties: null,
                                     body: body);

                Console.WriteLine("[x] Sent '{0}':'{1}'", topic, message);
            }

            //Console.WriteLine(" Press [enter] to exit.");
            //Console.ReadLine();
        }

        private static string GetMessage(string[] args)
        {
            return ((args.Length > 0)
                   ? string.Join(" ", args)
                   : "info: Hello World!");
        }
    }
}
